//
//  NYCSchoolDetail.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import Foundation

struct NYCSchoolDetail: Decodable {
    var id : String?
    var satTestTakers: String?
    var avgReadingScore: String?
    var avgMathsScore: String?
    var avgWritingScore: String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case satTestTakers = "num_of_sat_test_takers"
        case avgReadingScore = "sat_critical_reading_avg_score"
        case avgMathsScore = "sat_math_avg_score"
        case avgWritingScore = "sat_writing_avg_score"
    }

}
