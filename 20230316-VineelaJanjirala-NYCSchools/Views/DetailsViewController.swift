//
//  DetailsViewController.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import UIKit

class DetailsViewController: BaseViewController {

    var coordinator: SchoolDetailCooridnator?
    var schoolDetailViewModel: SchoolDetailViewModel?

    @IBOutlet weak var schoolDetailTableView: UITableView!

    func setViewModel(viewModel:SchoolDetailViewModel?) {
        self.schoolDetailViewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        self.navigationItem.title = Constants.schoolDetailNavigationTitle
        registerNibs()
    }
    
    func registerNibs() {
        schoolDetailTableView.register(Constants.schoolDescriptionCellIndentifier)
        schoolDetailTableView.register(Constants.schoolCellIndentifier)
        schoolDetailTableView.register(Constants.schoolCellScoreIndentifier)
        schoolDetailTableView.registerHeaderFooter(Constants.schoolDescriptionHeaderIndentifier)
    }
    
    func fetchData() {
        showLoader()
        schoolDetailViewModel?.fetchSchoolsDetails {
            [weak self] in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.schoolDetailTableView.reloadData()
                }
        }

        schoolDetailViewModel?.showError = { [weak self] errorText in
            DispatchQueue.main.async {
                self?.hideLoader()
            }
        }
    }
}

extension DetailsViewController : UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return schoolDetailViewModel?.getNumberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolDetailViewModel?.getNumberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = schoolDetailViewModel?.sections[indexPath.section]
        var tableViewCell: UITableViewCell?
        switch section {
            
        case .schoolInformation:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolCellIndentifier, for: indexPath) as? SchoolCell
            cell?.setDetailViewModel(viewModel: schoolDetailViewModel)
            cell?.delegate = self
            tableViewCell = cell
            break
        case .detail , .languages , .sports , .extraCuricularActivities:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolDescriptionCellIndentifier, for: indexPath) as? SchoolDescriptionCell
            cell?.setViewModel(viewModel: schoolDetailViewModel, index: indexPath.section)
            tableViewCell = cell
            break
        case .scores:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolCellScoreIndentifier, for: indexPath) as? SchoolScoreCell
            cell?.setViewModel(viewModel: schoolDetailViewModel)
            tableViewCell = cell
            break
        case .none:
            break
        }
        tableViewCell?.selectionStyle = .none
        return tableViewCell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.schoolDescriptionHeaderIndentifier) as! DetailHeaderView
        headerCell.setViewModel(viewModel: schoolDetailViewModel, index: section)
        return headerCell

    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.schoolDescriptionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension DetailsViewController: SchoolCellDelegate {
    func makeCall(phoneNumber: String?) {
        if let phoneNumber = phoneNumber , let url = URL(string: "tel://\(phoneNumber)") {
            let application = UIApplication.shared
            if (application.canOpenURL(url)) {
                application.open(url, options: [:], completionHandler: nil)
            }
            else {
                showError(text: Constants.phoneNumberInvalidError, style: .alert)
            }
        }
    }
    func composeMail(email: String?) {
        if let email = email , let url = URL(string: "email://\(email)") {
            let application = UIApplication.shared
            if (application.canOpenURL(url)) {
                application.open(url, options: [:], completionHandler: nil)
            }
            else {
                showError(text: Constants.phoneNumberInvalidError, style: .alert)
            }
        }
    }
}
