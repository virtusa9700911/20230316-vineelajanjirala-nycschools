//
//  DetailHeaderView.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//



import UIKit

class DetailHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var labelSectionTitle: UILabel!
    
    ///   Set the header section title
    /// - Parameter index: viewModel of the  SchoolDetailViewModel , index is the Indexpath.section
    func setViewModel(viewModel:SchoolDetailViewModel? , index: Int?) {
        labelSectionTitle.text = viewModel?.getSectionHeaderTitle(index: index)
    }


}
