//
//  SchoolDescriptionCell.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//



import UIKit

class SchoolDescriptionCell: UITableViewCell {

    @IBOutlet weak var labelDescription: UILabel!
    
    ///   setViewModel for the NYCSchoolModel description
    /// - Parameter index: viewModel of the  SchoolDetailViewModel
    func setViewModel(viewModel: SchoolDetailViewModel? , index: Int) {
        labelDescription.text = viewModel?.getDetailModel(index: index)
    }
     
}
