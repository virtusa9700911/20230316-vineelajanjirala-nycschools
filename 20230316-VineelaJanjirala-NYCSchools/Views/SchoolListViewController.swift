//
//  SchoolListViewController.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import UIKit

class SchoolListViewController: BaseViewController {

    var coordinator: SchoolListCoordinator?
    var viewModel: SchoolListViewModel?

    @IBOutlet weak var schoolTableView: UITableView!
    
    func setViewModel(viewModel: SchoolListViewModel?) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        self.navigationItem.title = Constants.schoolDashboardNavigationTitle
        schoolTableView.register(Constants.schoolCellIndentifier)
    }
    
    
    /// Fetch schools list from server on screen load
    func fetchData() {
        showLoader()
        viewModel?.fetchSchoolList {
            [weak self] in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    // Setting the passThroughData [NYCSchool] for the selected School data
                    self?.coordinator?.passThroughData = self?.viewModel?.schoolModels
                    self?.schoolTableView.reloadData()
                }
        }
        
        viewModel?.showError = { [weak self] errorText in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.showError(text: errorText, style: .placeHolder)
            }
        }
    }
    
}

///Table view data source & delegate methods
extension SchoolListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getNumberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolCellIndentifier, for: indexPath) as? SchoolCell else { return UITableViewCell()}
        cell.setViewModel(viewModel: viewModel, index: indexPath.row)
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.tertiarySystemGroupedBackground : UIColor.white
        cell.delegate = self
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.coordinator?.coordianteToDetails(index: indexPath.row)
    }
    
}


extension SchoolListViewController: SchoolCellDelegate {
    
    /// Open phone application using URL pattern
    /// - Parameter phoneNumber: phone number of the school
    func makeCall(phoneNumber: String?) {
        if let phoneNumber = phoneNumber , let url = URL(string: "tel://\(phoneNumber)") {
           let application = UIApplication.shared
           if (application.canOpenURL(url)) {
               application.open(url, options: [:], completionHandler: nil)
           }
            else {
                showError(text: Constants.phoneNumberInvalidError, style: .alert)
            }
         }
    }
    
    /// Open email application using URL pattern
    /// - Parameter email: email address of the school
    func composeMail(email: String?) {
        if let email = email , let url = URL(string: "mailto://\(email)") {
            let application = UIApplication.shared
            if (application.canOpenURL(url)) {
                application.open(url, options: [:], completionHandler: nil)
            }
            else {
                showError(text: Constants.phoneNumberInvalidError, style: .alert)
            }
        }
    }
}
