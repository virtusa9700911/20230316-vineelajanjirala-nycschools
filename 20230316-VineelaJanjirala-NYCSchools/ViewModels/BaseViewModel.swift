//
//  BaseViewModel.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import UIKit


class BaseViewModel {
    /// Throwing  the Error to the View after the service call
    var showError: ((String) -> Void)?
}
    
