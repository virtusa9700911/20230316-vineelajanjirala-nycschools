//
//  SchoolDetailCoordinator.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import Foundation

/// Coordinator for the NYCSchoolDetailVC
///

class SchoolDetailCooridnator: BaseCoordinator {
    func start(passThoughData:NYCSchool?) {
        let viewModel = SchoolDetailViewModel(passThroughData: passThoughData)
        let vc = DetailsViewController.loadVCFromStoryBoard()
        vc.setViewModel(viewModel: viewModel)
        vc.coordinator = self
        navigationController?.pushViewController(vc, animated: true)
    }
}
