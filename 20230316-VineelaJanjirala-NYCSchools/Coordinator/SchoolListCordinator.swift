//
//  SchoolListCordinator.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import Foundation

/// Coordinator for the NYCSchoolDasboardViewController
///
class SchoolListCoordinator: BaseCoordinator {
    
    var passThroughData: [NYCSchool]?

    override func start() {
        let viewModel = SchoolListViewModel()
        let vc = SchoolListViewController.loadVCFromStoryBoard()
        vc.setViewModel(viewModel: viewModel)
        vc.coordinator = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    /// <#Description#>
    /// - Parameter index: <#index description#>
    func coordianteToDetails(index:Int?) {
        if let index = index {
            let coordinator = SchoolDetailCooridnator(navigationController: navigationController)
            coordinator.start(passThoughData: passThroughData?[index])
        }
    }
}
