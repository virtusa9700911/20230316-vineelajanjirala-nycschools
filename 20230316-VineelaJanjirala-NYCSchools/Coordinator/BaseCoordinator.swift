//
//  BaseCoordinator.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//


import UIKit

protocol Coordinator {
    var navigationController: UINavigationController? { get set }
    func start()
}

class BaseCoordinator: Coordinator {
    
    weak var navigationController: UINavigationController?

    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func start() {
        let coordinator = SchoolListCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    
}
