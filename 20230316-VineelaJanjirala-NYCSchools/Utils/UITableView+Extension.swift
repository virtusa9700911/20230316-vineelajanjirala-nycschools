//
//  UITableView+Extension.swift
//  20230316-VineelaJanjirala-NYCSchools
//
//  Created by Vineela Janjirala on 3/16/23.
//

import UIKit

extension UITableView {
    
    func registerHeaderFooter(_ nibName: String) {
        register(UINib.init(nibName: nibName, bundle: nil), forHeaderFooterViewReuseIdentifier: nibName)
    }
    
    func register(_ nibName: String) {
        register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
    }
}
