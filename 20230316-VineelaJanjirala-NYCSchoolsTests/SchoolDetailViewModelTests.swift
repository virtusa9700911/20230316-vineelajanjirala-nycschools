//
//  SchoolDetailViewModelTests.swift
//  20230316-VineelaJanjirala-NYCSchoolsTests
//
//  Created by Vineela Janjirala on 3/16/23.
//



import XCTest
@testable import _0230316_VineelaJanjirala_NYCSchools

class SchoolDetailViewModelTests: XCTestCase {

    var listViewModel: SchoolListViewModel!
    var viewModel: SchoolDetailViewModel!

    override func setUp() {
        listViewModel = SchoolListViewModel()
        self.viewModel = SchoolDetailViewModel(passThroughData: nil)
        let exp = expectation(description: "Loading school details")
        APIHelper.getData(path: .getList) { data, error in
            self.listViewModel.schoolModels = try? JSONDecoder().decode([NYCSchool].self, from: data!)
            
        //initializing SchoolDetailViewModel with random school data
            self.viewModel.schoolModel = self.listViewModel.schoolModels?.randomElement()
            exp.fulfill()
        }
                        
        let exp1 = expectation(description: "Loading school details")
        APIHelper.getData(path: .getDetails) { data1, error1 in
            self.viewModel.schoolsDetailModels = try? JSONDecoder().decode([NYCSchoolDetail].self, from: data1!)
            exp1.fulfill()
        }
        
        waitForExpectations(timeout: 5)
//        wait(for: [exp, exp1], timeout: 5)
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMathsScore(){
        XCTAssertNotEqual(self.viewModel.mathsScore, "", "Maths score not available for \(self.viewModel.schoolName)")
    }
    
    func testWritingScore(){
        XCTAssertNotEqual(self.viewModel.writingScore , "", "Writing score not available for \(self.viewModel.schoolName)")
    }
    
    func testReadingScore(){
        XCTAssertNotEqual(self.viewModel.readingScore, "",  "Reading score not available for\(self.viewModel.schoolName)")
    }
    
    func testSports(){
        XCTAssertNotEqual(self.viewModel.sports, "" , "Sports not available for \(self.viewModel.schoolName)")
    }
    
    func testExtraCuricularActivities(){
        XCTAssertNotEqual(self.viewModel.extraCuricularActivities, "", " ExtraCuricular Activities are not available for \(self.viewModel.schoolName)")
    }
    
    func testLanguages(){
        XCTAssertNotEqual(self.viewModel.languages, "" , "Languages data not available for \(self.viewModel.schoolName)")
    }

}
